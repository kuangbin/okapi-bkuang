package net.sf.okapi.connectors.google;

import java.util.List;

import net.sf.okapi.common.Util;
import net.sf.okapi.common.query.MatchType;
import net.sf.okapi.common.query.QueryResult;

abstract class QueryResultBuilder<T> {
    protected GoogleMTv2Parameters params;
    protected String name;
    protected int weight;

    QueryResultBuilder(GoogleMTv2Parameters params, String name, int weight) {
        this.params = params;
        this.name = name;
        this.weight = weight;
    }

    abstract List<QueryResult> convertResponses(List<TranslationResponse> responses, T sourceContent);

    protected QueryResult createQueryResult(TranslationResponse response) {
        QueryResult qr = new QueryResult();
        qr.setFuzzyScore(95); // Arbitrary result for MT
        qr.setCombinedScore(95);
        qr.weight = weight;
        qr.origin = name;
        qr.matchType = MatchType.MT;
        return qr;
    }
}