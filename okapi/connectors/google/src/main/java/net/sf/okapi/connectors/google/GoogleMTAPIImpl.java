package net.sf.okapi.connectors.google;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.okapi.common.StreamUtil;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.exceptions.OkapiException;

public class GoogleMTAPIImpl implements GoogleMTAPI {
    private static final Logger LOG = LoggerFactory.getLogger(GoogleMTAPIImpl.class);

    private final String baseUrl;
    private GoogleMTv2Parameters params;
    private GoogleResponseParser parser = new GoogleResponseParser();

    public GoogleMTAPIImpl(String baseUrl, GoogleMTv2Parameters params) {
        this.baseUrl = baseUrl;
        this.params = params;
    }

    @Override
    public List<TranslationResponse> translate(String srcCode, String tgtCode, List<String> sourceTexts)
                                        throws IOException, ParseException {
        QueryBuilder qb = new QueryBuilder(srcCode, tgtCode);
        List<TranslationResponse> responses = new ArrayList<>();
        for (String sourceText : sourceTexts) {
            if (qb.hasCapacity(sourceText)) {
                qb.addQuery(sourceText);
            }
            else {
                if (qb.getSourceCount() > 0) {
                    LOG.debug("Flushing batch query of length {}, '{}'", qb.getQuery().length(), qb.getQuery());
                    responses.addAll(query(qb));
                    qb.reset();
                }
                if (qb.hasCapacity(sourceText)) {
                    qb.addQuery(sourceText);
                }
                else {
                    // If we still don't have capacity, it's an oversized segment that needs to be POSTed by
                    // itself.
                    responses.add(querySingleSegment(qb, sourceText));
                }
            }
        }
        if (qb.getSourceCount() > 0) {
            responses.addAll(query(qb));
        }
        return responses;
    }

    @Override
    public List<String> getLanguages() throws IOException, ParseException {
        URL url = new URL(baseUrl + "/languages?key=" + params.getApiKey());
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        int code = conn.getResponseCode();
        if ( code == 200 ) {
            return parser.parseLanguagesResponse(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8));
        }
        else {
            String errorBody = StreamUtil.streamUtf8AsString(conn.getErrorStream());
            String error = parser.parseError(new StringReader(errorBody));
            LOG.error("Error {} - {} for query {}; full response: {}",
                      code, error, url.toString(), errorBody);
            throw new OkapiException(String.format("Error: response code %d - %s\n"
                                     + conn.getResponseMessage(), code, error));
        }
    }

    private List<TranslationResponse> query(QueryBuilder qb) throws IOException, ParseException  {
        URL url = new URL(qb.getQuery());
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        int code = conn.getResponseCode();
        if ( code == 200 ) {
            List<String> translatedTexts = 
                    parser.parseResponse(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8));
            List<TranslationResponse> responses = new ArrayList<>();
            if (qb.getSourceCount() != translatedTexts.size()) {
                LOG.error("Received {} translations for {} sources in query {}", translatedTexts.size(),
                          qb.getSourceCount(), qb.getQuery());
                throw new OkapiException("API returned incorrect number of translations (expected " +
                          qb.getSourceCount() + ", got " + translatedTexts.size());
            }
            for (int i = 0; i < qb.getSourceCount(); i++) {
                responses.add(new TranslationResponse(qb.getSources().get(i), translatedTexts.get(i)));
            }
            return responses;
        }
        else {
            String errorBody = StreamUtil.streamUtf8AsString(conn.getErrorStream());
            String error = parser.parseError(new StringReader(errorBody));
            LOG.error("Error {} - {} for query {}; full response: {}",
                      code, error, qb.getQuery(), errorBody);
            throw new OkapiException(String.format("Error: response code %d - %s\n"
                                     + conn.getResponseMessage(), code, error));
        }
    }

    @SuppressWarnings("unchecked")
    private TranslationResponse querySingleSegment(QueryBuilder qb, String sourceText)
                                            throws IOException, ParseException {
        LOG.debug("Using POST query for source '{}...' of length {}", sourceText.substring(0, 32), sourceText.length());
        URL url = new URL(qb.getQuery());
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setDoOutput(true);

        JSONObject json = new JSONObject();
        json.put("q", sourceText);
        try (OutputStreamWriter w = new OutputStreamWriter(conn.getOutputStream(), StandardCharsets.UTF_8)) {
            w.write(json.toJSONString());
        }
        GoogleResponseParser parser = new GoogleResponseParser();
        int code = conn.getResponseCode();
        if ( code == 200 ) {
            List<String> translatedTexts =
                    parser.parseResponse(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8));
            if (translatedTexts.size() != 1) {
                LOG.error("Received {} translations for {} sources in POST query {} with body '{}'", translatedTexts.size(),
                          1, qb.getQuery(), sourceText);
                throw new OkapiException("API returned incorrect number of translations (expected 1, got " +
                              translatedTexts.size());
            }
            return new TranslationResponse(sourceText, translatedTexts.get(0));
        }
        else {
            String errorBody = StreamUtil.streamUtf8AsString(conn.getErrorStream());
            String error = parser.parseError(new StringReader(errorBody));
            LOG.error("Error {} - {} for query {}; full response: {}",
                      code, error, qb.getQuery(), errorBody);
            throw new OkapiException(String.format("Error: response code %d - %s\n"
                                     + conn.getResponseMessage(), code, error));
        }
    }

    class QueryBuilder {
        // "The URL for GET requests, including parameters, must be less than 2K characters."
        // https://cloud.google.com/translate/docs/translating-text#translating_text_1
        private static final int QUERY_LIMIT = 2048;
        private static final String QPARAM = "&q=";

        private StringBuilder sb;
        private final String srcCode, tgtCode;
        private List<String> sources = new ArrayList<>();

        public QueryBuilder(String srcCode, String tgtCode) {
            this.srcCode = srcCode;
            this.tgtCode = tgtCode;
            reset();
        }

        public void reset() {
            sb = new StringBuilder(baseUrl)
                .append("?key=").append(params.getApiKey())
                .append("&source=").append(srcCode)
                .append("&target=").append(tgtCode);
            sources.clear();
        }

        public boolean hasCapacity(String sourceText) {
            int additionalLen = QPARAM.length() + Util.URLEncodeUTF8(sourceText).length();
            return (sb.length() + additionalLen < QUERY_LIMIT);
        }

        public void addQuery(String sourceText) {
            if (!hasCapacity(sourceText)) {
                throw new IllegalStateException("Query too long to add '" + sourceText + "'");
            }
            sb.append(QPARAM).append(Util.URLEncodeUTF8(sourceText));
            sources.add(sourceText);
        }

        public List<String> getSources() {
            return sources;
        }
        public int getSourceCount() {
            return sources.size();
        }

        public String getQuery() {
            return sb.toString();
        }
    }
}
